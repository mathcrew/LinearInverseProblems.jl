using Documenter, LinearInverseProblems

makedocs(
    modules = [LinearInverseProblems],
    format = Documenter.HTML(; prettyurls = get(ENV, "CI", nothing) == "true"),
    sitename = "LinearInverseProblems.jl",
    pages = [
        "Home"     => "index.md",
        "Usage"    => "usage.md",
        "Solver"   => "solver.md",
        "Examples" => "examples.md",
        "Glossary" => Any[
                    "functions.md",
                    "types.md",
                   ],
    ],
    doctest = false
)
