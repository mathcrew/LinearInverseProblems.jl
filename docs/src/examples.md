# Examples

```jldoctest
julia> using LinearInverseProblems

julia> x = [1.0; 0.0; 0.0];

julia> A = [1.0 0.0 0.0
            0.0 1.0 0.0];

julia> y = A*x;

julia> omp(y, A, s=1) == x
true
```

 ```jldoctest
julia> using LinearAlgebra, Random, LinearInverseProblems

julia> Random.seed!(123);

julia> A = sqrt(1/30)*randn(30,50);

julia> x = zeros(50); x[randperm(50)[1:10]].=randn(10); y = A*x;

julia> s = Homotopy(y, A, maxiter=500);

julia> for v in s
           v<1e-5 && break
       end

julia> norm(s.x-x)<1e-5
true

julia> copyto!(s.y, A*ones(size(x))); # not sparse

julia> collect(s); norm(s.x-x)<1e-3
false
```
