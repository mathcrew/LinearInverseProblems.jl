# Solvers

| Name      | Type   | Solves        | Ref  |
| :-------: | :------: |:----------: | :--: |
| FISTA     | `FISTA`  | ``\ell_1``  |      |
