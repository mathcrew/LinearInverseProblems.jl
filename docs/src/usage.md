# Usage

Every solver has a corresponding type (see [Solvers](@ref)) e.g. `OMP`. Each iterative algorithm uses the julia [iteration interface](https://docs.julialang.org/en/stable/manual/interfaces/#man-interface-iteration-1) and returns the residual (``\lVert A x_k - y \rVert``) or some constant if `verbose=false`.

Each solver also has a utility functions which solves one problem and returns the estimate of the solution.

See [Examples](@ref) for a list of examples.
