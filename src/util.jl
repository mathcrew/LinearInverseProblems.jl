"""
sparsity(x::AbstractArray{T}, tol=sqrt(eps(T)))

count elements of x that are smaller than tol
"""
sparsity(x::AbstractArray{T}, tol=sqrt(eps(T))) where T = count(i -> abs(i)>tol, x)

"""
`transform(A, f)`

Returns the matrix `AΨ`, where `f` defines the adjoint basis transform of an orthogonal basis Ψ.  
"""
function transform(A::AbstractMatrix{T}, transformation) where T
    B = similar(A)
    v = zeros(T, size(A,2))
    w = similar(v)
    for k=1:size(B,1)
        av = @view A[k,:]
        bv = @view B[k,:]
        copyto!(w, av); copyto!(v, bv)
        v .= transformation(w)
        copyto!(bv, v)
    end
    return B
end
