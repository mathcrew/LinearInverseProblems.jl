using ImageFiltering.KernelFactors: gaussian
using ImageFiltering: imfilter
export SoftThreshold, SVDDenoise, GaussianFilter

soft(x, τ) = abs(x) > τ ? (abs(x) - τ)*sign(x) : zero(typeof(x))
soft_plus(x, τ) = (x-τ)>0 ? x : zero(typeof(x))

struct SoftThreshold
end

denoise(x::AbstractArray, d::SoftThreshold, σ) = soft.(x, σ)

function divergence(x::AbstractArray, d::SoftThreshold, σ)
    return oneunit(eltype(x)) * count(i -> abs(x[i]) > σ, 1:length(x))
end


struct SVDDenoise
end

function denoise(X::AbstractMatrix{T}, d::SVDDenoise, σ::T) where T
    F = svd!(X)
    return F.U*Diagonal(soft_plus(F.S, σ))*F.Vt
end

function divergence(X::AbstractMatrix{T}, d::SVDDenoise, σ::T) where T
    sval = svdvals!(X)
    s = zero(T, 0)
    for i=1:length(svals)
        @inbounds s_i = svals[i]*soft_plus(svals[i], σ)
        for j=1:length(svals)
            if i != j
                @inbounds s += s_i/(svals[i]^2 - svals[j]^2)
            end
        end
    end
    return count(i->svals[i]>σ) + 2*s
end


struct GaussianFilter{T<:Union{Nothing, Integer}}
    k::T
end
GaussianFilter() = GaussianFilter(nothing)

function denoise(x::AbstractArray, d::GaussianFilter{Integer}, σ)
    g = gaussian(σ, d.k)
    return imfilter(x, g)
end

function denoise(x::AbstractArray, d::GaussianFilter{Nothing}, σ)
    g = gaussian(σ)
    return imfilter(x, g)
end

function divergence(x::AbstractArray, d::GaussianFilter, σ, ε=1e-3, M=1)
    divergence = 0
    for i = 1:M
        b = randn(size(x))
        divergence += sum(b .* (denoise(x .+ ε.*b, d, σ) .- denoise(x, d, σ)))
    end
    return divergence / (M*ε)
end
