mutable struct DenoisingAMP{T<:Number, R<:AbstractArray{T}}
    y::R
    A
    x::R
    D
    maxiter::Int
    u::R
    w::R
    z::R
    σ::T
    verbose::Bool
    function DenoisingAMP(y::AbstractArray{T}, A, D;
                          maxiter=1000,
                          x::AbstractArray{T}=zeros(T, size(A, 2)),
                          verbose=true) where T
        size(A) == (length(y), length(x)) || throw(DimensionMismatch())
        u = zeros(size(x))
        w = similar(y)
        z = deepcopy(y)
        σ = rand()
        mul!(u, A', z)
        return new{T, typeof(y)}(y, A, x, D, maxiter, u, w, z, σ, verbose)
    end
end

function iterate!(solver::DenoisingAMP{T, R}) where {T, R}
    x = solver.x; y = solver.y; z = solver.z; w = solver.w; u = solver.u
    x_prev = deepcopy(x)
    m = length(y)
    mul!(w, solver.A, x)
    div = divergence(x + u, solver.D, solver.σ) / m
    @. z = y - w + z * div
    solver.σ = norm(z) / sqrt(m)
    mul!(u, solver.A', z)
    x .= denoise(x + u, solver.D, solver.σ)
    v = solver.verbose ? norm(x-x_prev) : -1 * one(T)
    return v
end

Base.iterate(S::DenoisingAMP, state=1) = state > S.maxiter ? nothing : (iterate!(S), state+1)
Base.eltype(::Type{DenoisingAMP{T,R}}) where {T,R} = T
Base.length(S::DenoisingAMP) = S.maxiter

"""
denoisingAMP(y, A, D) -> x

[...]

Reference:
Metzler C. A., Maleki A., Baraniuk R. G.: From Denoising to Compressed Sensing.
"""

function denoisingAMP(y::AbstractArray{T}, A, D; kwargs...) where T<:Number
    size(A, 1) == length(y) || throw(DimensionMismatch())
    solver = DenoisingAMP(y, A, D; kwargs...)
    for v in solver
        isapprox(v, zero(T)) && break
    end
    return solver.x
end
