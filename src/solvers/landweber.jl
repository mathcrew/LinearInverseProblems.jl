mutable struct Landweber{T<:Number, R<:AbstractArray{T}}
    y::R
    A
    x::R
    ω::T
    maxiter::Int
    z::R
    w::R
    verbose::Bool
    function Landweber(y::AbstractArray{T}, A, ω; x::AbstractArray{T}=zeros(T, size(A,2)), maxiter=1000, verbose=true) where T
        size(A) == (length(y), length(x)) || throw(DimensionMismatch())
        z = similar(x); w = similar(y)
        return new{T,typeof(y)}(y, A, x, T(ω), maxiter, z, w, verbose)
    end
end

function iterate!(solver::Landweber{T,R}) where {T,R}
    A = solver.A; x = solver.x; y = solver.y
    copyto!(solver.z, x)
    mul!(solver.w, A, x)
    solver.w .= solver.w .- y
    mul!(x, A', solver.w)
    x .= solver.z .- solver.ω*x
    v = solver.verbose ? norm(x-solver.z) : -1 * one(T)
    return v
end

Base.iterate(S::Landweber, state=1) = state > S.maxiter ? nothing : (iterate!(S), state+1)
Base.eltype(::Type{Landweber{T,R}}) where {T,R} = T
Base.length(S::Landweber) = S.maxiter

function landweber(y, A, ω::Real; maxiter::Int, tol=isapprox) where T<:Number
    solver = Landweber(y, A, ω, maxiter=maxiter, verbose=true)
    for v in solver
        isapprox(v, zero(eltype(y))) && break
    end
    return solver.x
end
