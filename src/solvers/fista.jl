mutable struct FISTA{T<:Number, R<:AbstractArray{T}} <: AbstractSparseSolver
    y::R
    A
    x::R
    L::T
    λ::T
    maxiter::Int
    z::R
    w::R
    t0::T
    t1::T
    verbose::Bool
    function FISTA(y::AbstractArray{T}, A, λ=T(0.05), L::T=2*opnorm(A)^2; maxiter=1000, x::AbstractArray{T}=zeros(T, size(A,2)), verbose=true) where T
        size(A) == (length(y), length(x)) || throw(DimensionMismatch())
        return new{T,typeof(y)}(y, A, x, L, T(λ), maxiter, similar(x), similar(y), one(T), one(T), verbose)
    end
end

function iterate!(solver::FISTA{T,R}) where {T,R}
    solver.t0 = solver.t1
    x = solver.x; y = solver.y; z = solver.z; w = solver.w
    copyto!(z, x)
    mul!(w, solver.A, x)
    mul!(x, solver.A', w-y)
    # proximity operator
    x .= z.- 2/solver.L * x
    softthreshold!(x, solver.λ/solver.L)
    # update stepsize and result
    solver.t1 = (1 + sqrt(1+4*solver.t0^2)) / 2
    x .= x .+ (solver.t0- 1) / solver.t1 * (x .- z)
    v = solver.verbose ? norm(x-z) : -1 * one(T)
    return v
end

Base.iterate(S::FISTA, state=1) = state > S.maxiter ? nothing : (iterate!(S), state+1)
Base.eltype(::Type{FISTA{T,R}}) where {T,R} = T
Base.length(S::FISTA) = S.maxiter

function softthreshold!(x, λ::Real)
    x .= max.(0, abs.(x) .- λ) .* sign.(x)
end

"""
fista(y, A, λ, L) -> x

Fast iterative shrinkage-thresholding algorithm (FISTA) to find `x` in a linear
inverse problem `A*x=y`.

Reference: 
Beck A., Teboulle M.: A Fast Iterative Shrinkage-Thresholding Algorithm for
Linear Inverse Problems
"""
function fista(y::AbstractArray{T}, A, λ::Real, L::Real; maxiter::Int=1000, tol=isapprox) where T<:Number
    (size(A, 1) == length(y)) || throw(DimensionMismatch())
    solver = FISTA(y, A, λ, L, maxiter=maxiter, verbose=true)
    for v in solver
        isapprox(v, zero(T)) && break
    end
    return solver.x
end
