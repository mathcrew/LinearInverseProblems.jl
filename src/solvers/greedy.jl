"""
OMP{T<:Number}

OMP iterable solver structure. Has the fields:
```
A # the measurement matrix, needs to support mul!/size operations
x # variable storing the iteration sequence
y # measurements
z, w # tmp variables
s # sparsity level
S # sparsity support
verbose # wheter to calculate residual during iterations
```
"""
mutable struct OMP{T<:Number, R<:AbstractArray{T}} <: AbstractSparseSolver
    y::R
    A
    x::R
    z::R
    w::R
    s::Int
    S::Vector{Int}
    verbose::Bool
    function OMP(y::AbstractArray{T}, A; x::AbstractArray{T}=zeros(T, size(A,2)), s::Int=round(Int, 0.1*size(A,1)), verbose=true) where T
        size(A) == (length(y), length(x)) || throw(DimensionMismatch())
        s < size(A, 2) || throw("The sparsity needs to be smaller than $(size(A, 2))")
        return new{T,typeof(y)}(y, A, x, similar(x), similar(y), s, Int[], verbose)
    end
end

function iterate!(solver::OMP{T,R}) where {T,R}
    mul!(solver.w, solver.A, solver.x)
    solver.w .= solver.w .- solver.y
    mul!(solver.z, solver.A', solver.w)
    indexmax = argmax(abs.(solver.z))
    (indexmax in solver.S) || push!(solver.S, indexmax)
    A_S = @view solver.A[:,solver.S]
    solver.x[solver.S] .= A_S \ solver.y
    v = solver.verbose ? norm(solver.w) : -1 * one(T)
    return v # norm(w)=norm(A*x_{k-1}-y)
end

Base.iterate(solver::OMP, state=1) = state > solver.s ? nothing : (iterate!(solver), state+1)
Base.eltype(::Type{OMP{T,R}}) where {T,R} = T
Base.length(solver::OMP) = solver.s

"""
omp(y; A=I, x0=zeros(size(A,2)), s::Int=size(A,1)) -> x

Orthogonal matching pursuit (OMP) to find the `s`-sparse vector `x` that solves `A*x=y`.
Uses the linear solver (`\\`) in each iteration (i.e. QR) and is thus not very efficient. Since QR factorization updates are not yet implemented (see: [julia/#20791](https://github.com/JuliaLang/julia/issues/20791))
"""
function omp(y, A; x0=zeros(size(A, 2)), s::Int=size(A, 1))
    (size(A, 1) == length(y)) || throw(DimensionMismatch())
    (s < size(A, 2)) || throw("The sparsity needs to be smaller than $(size(A, 2))")
    solver = OMP(y, A, x=x0, s=s, verbose=false)
    V = collect(solver)
    return solver.x
end
