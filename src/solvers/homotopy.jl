mutable struct Homotopy{T<:Number} <: AbstractSparseSolver
    y::AbstractArray{T}
    A::AbstractMatrix{T}
    x::AbstractArray{T}
    maxiter::Int
    c::AbstractArray{T}
    S::Vector{Int}
    z::AbstractArray{T}
    verbose::Bool
    λ::T
    γ::T
    function Homotopy(y::AbstractArray{T}, A; maxiter::Int=1000, verbose::Bool=true) where T
        x = zeros(T, size(A,2))
        λ = zero(T); S = Int[]
        c = similar(x)
        γ = zero(T); z = similar(x)
        return new{T}(y, A, x, maxiter, c, S, z, verbose, λ, γ)
    end
end

function first_iterate!(solver::Homotopy{T}) where T
    A = solver.A
    mul!(solver.c, A', solver.y)
    (solver.λ, l) = findmax(abs.(solver.c))
    (l in solver.S) || push!(solver.S, l)
    d = sign(solver.c[l])/norm(A[:,l])^2
    solver.z = A'*A[:,l]*d
    (solver.γ, l) = gamma_plus(solver.z, solver.c, solver.λ, solver.S)
    solver.x[solver.S] .= solver.γ*d
    (l in solver.S) || push!(solver.S, l)
    v = solver.verbose ? norm(solver.c) : -1 * one(T)
    return v
end

function iterate!(solver::Homotopy{T}) where T
    A = solver.A; y = solver.y; x = solver.x; z = solver.z;
    c = solver.c; S = solver.S
    c = A'*(A*x - y)
    solver.λ = norm(c, Inf)
    # that's definitely not optimal ;)
    B = A[:,S]'*A[:,S]
    d = B\(-sign.(c[S]))
    z = A'*A[:,S]*d
    (γp,l1) = gamma_plus(z, c, solver.λ, S)
    (γm,l2) = gamma_minus(x, d, S)
    solver.γ = min(γp, γm)
    x[S] .= x[S] .+ solver.γ*d
    if γp<γm
        if !(l1 in S)
            push!(S, l1)
        end
    elseif γp==γm
        i = findnext(k->k==l2, S, 1)
        solver.S = i==nothing ? S : remove(S, i)
    end
    v = solver.verbose ? norm(solver.c) : -1 * one(T)
    return v
end

function Base.iterate(solver::Homotopy, state=1)
    if state==1
        return (first_iterate!(solver), state+1)
    elseif state<=solver.maxiter
        return (iterate!(solver), state+1)
    else
        return nothing
    end
end

Base.eltype(::Type{Homotopy{T}}) where T = T
Base.length(solver::Homotopy) = solver.maxiter

function homotopy(y, A; maxiter=100, tol=sqrt(eps()))
    (size(A, 1) == length(y)) || throw(DimensionMismatch())
    solver = Homotopy(y, A, maxiter=maxiter, verbose=true)
    for v in solver
        v>=tol || break
    end
    return solver.x
end

function gamma_plus(v::AbstractArray{T}, c::AbstractArray{T}, λ::T, S::Array{Int}) where T
    res1 = typemax(T); res2 = res1; index = 0
    for i=1:length(v)
        if !(i in S)
            t = (λ+c[i])/(1-v[i])
            m1 = t>0 ? t : typemax(T)
            t = (λ-c[i])/(1+v[i])
            m2 = t>0 ? t : typemax(T)
            res1 = min(m1,m2)
            if res1<res2
                res2 = res1
                index = i
            end
        end
    end
    # necessary when we use Homotopy in Rembo
    index = index == 0 ? S[1] : index
    return (res2,index)
end

function gamma_minus(x::AbstractArray{T}, d::AbstractArray{T}, S::Array{Int}) where T
    res1 = typemax(T); res2 = res1; index=0
    for j=1:length(S)
        t = -x[S[j]]/d[j]
        if !iszero(d[j]) && t>0
            res1 = min(res1, t)
            if res1<res2
                res2 = res1
                index = j
            end
        end
    end
    return (res2,index)
end

"""
removes the `i`th element of the iterable `itr`
"""
function remove(itr, i)
    x = zeros(eltype(itr),length(itr)-1)
    if i==1
        x .= itr[2:end]
    elseif i==length(itr)
        x .= itr[1:end-1]
    else
        x .= itr[[1:i-1;i+1:length(itr)]]
    end
    return x
end
