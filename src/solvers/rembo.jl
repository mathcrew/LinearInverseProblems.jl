mutable struct Rembo{T<:Number, R<:AbstractArray{T}}
    Y::R
    A
    X::R
    maxiter::Int
    ε::T
    K::Int
    D::Distribution{Univariate, Continuous}
    S::AbstractSparseSolver
    verbose::Bool
    function Rembo(Y::AbstractArray{T}, A;
                   X::AbstractArray{T}=zeros(T, (size(A, 2), size(Y, 2))),
                   S::AbstractSparseSolver=OMP(zeros(T, size(A,1)), A, verbose=false),
                   maxiter=1000,
                   ε=10e-4, K=10,
                   D=Uniform(-1, 1),
                   verbose=false) where T
        size(A) == (size(Y, 1), size(X, 1)) || throw(DimensionMismatch())
        return new{T, typeof(Y)}(Y, A, X, maxiter, ε, K, D, S, verbose)
    end
end

function iterate!(solver::Rembo{T, R}) where R where T
    A = solver.A; Y = solver.Y; X = solver.X
    D = solver.D

    a = rand(D, size(Y, 2))
    y = Y * a

    # solve SMV
    copyto!(solver.S.y, y)
    collect(solver.S)
    x̂ = solver.S.x
    support = findall(!iszero, x̂)

    # ToDo: isapprox strange behaviour for Homotopy (not fullfilled but A*X≈Y)
    if length(support) <= solver.K && isapprox(y, A * x̂, atol=solver.ε)
        flag = false # indicate finish
    else
        flag = true
    end
    X .= 0
    X[support, :] .= A[:, support]\Y
    v = solver.verbose ? norm(x-z) : -1 * one(T)
    return flag
end


Base.iterate(S::Rembo, state=1) = state > S.maxiter ? nothing : (iterate!(S), state+1)
Base.eltype(::Type{Rembo{T,R}}) where {T,R} = T
Base.length(S::Rembo) = S.maxiter

"""
rembo(Y, A, ε, K::Int, maxiter::Int, ...)

See Rembo for further informations
"""
function rembo(Y, A; kwargs...) where T<:Number
    (size(A, 1) == size(Y, 1)) || throw(DimensionMismatch())
    solver = Rembo(Y, A; kwargs...) 
    # high-level API
    for flag in solver
        !flag && break
        #isapprox(v, zero(eltype(v))) && break
    end
    return solver.X
end
