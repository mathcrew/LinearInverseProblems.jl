mutable struct IRLS{T<:Number, R<:AbstractArray{T}}
    y::R
    A
    p::Int
    pk::T
    K::T
    x::R
    maxiter::Int
    z::R
    w::R
    verbose::Bool
    function IRLS(y::AbstractArray{T}, A; p=2, pk=2, K=1.2, x::AbstractArray{T}=zeros(T, size(A, 2)), maxiter=1000, verbose=true) where T
        size(A) == (length(y), length(x)) || throw(DimensionMismatch())
        return new{T,typeof(y)}(y, A, p, pk, K, x, maxiter, similar(x), similar(x), verbose)
    end
end

function iterate!(solver::IRLS{T,R}) where {T,R}
    y = solver.y; A = solver.A; p = solver.p; pk = solver.pk; x = solver.x; z = solver.z; w = solver.w;
    copyto!(z, x)
    pk = p>=2 ? min(p, solver.K*pk) : max(p, solver.K*pk)
    W = Diagonal(sqrt.(abs.(x).^2 .+ sqrt(eps(T))))
    # Minimum weighted norm solution.
    x .= W*W'*A' * ((A*W*W'*A')\y)
    q = 1/(pk-1)
    if p>=2
        x .= q*x + (1-q)*z
    end
    v = solver.verbose ? norm(x-z) : -1 * one(T)
    return v
end

Base.iterate(S::IRLS, state=1) = state > S.maxiter ? nothing : (iterate!(S), state+1)
Base.eltype(::Type{IRLS{T,R}}) where {T,R} = T
Base.length(S::IRLS) = S.maxiter

"""
irls(y, A, p) -> x

Iteratively Reweighted Least Squares algorithm (IRLS) to find `x` in a linear inverse problem `A*x=y`.

Reference: Foucart, Rauhut - A Mathematical introduction to compressed Sensing (page 493)
"""
function irls(y::AbstractArray{T}, A; p::Int=2, K::T=1.2, maxiter::Int=1000, tol=isapprox) where T<:Number
    (size(A, 1) == length(y)) || throw(DimensionMismatch())
    solver = IRLS(y, A, p=p, maxiter=maxiter, verbose=true)
    for v in solver
        isapprox(v, zero(T)) && break
    end
    return solver.x
end
