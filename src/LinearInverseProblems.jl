module LinearInverseProblems

using LinearAlgebra, Random, Distributions

export omp, landweber, fista, homotopy, irls, rembo, denoisingAMP,
       OMP, Landweber, FISTA, Homotopy, IRLS, Rembo, DenoisingAMP

abstract type AbstractSparseSolver end

export denoise, divergence

include("util.jl")
include("denoisers.jl")
include("solvers/greedy.jl")
include("solvers/landweber.jl")
include("solvers/fista.jl")
include("solvers/homotopy.jl")
include("solvers/irls.jl")
include("solvers/damp.jl")
include("solvers/rembo.jl")

end
