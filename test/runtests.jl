import FFTW: dct, idct
using LinearAlgebra, Random
using LinearInverseProblems
using Test
using Documenter: doctest

Random.seed!(42)

include("solver.jl")

@testset "Doctests" begin doctest(LinearInverseProblems) end

println("\nSuccess!")
