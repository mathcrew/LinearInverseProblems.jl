@testset "Solvers" begin

@testset "Single measurement" begin
rng = Random.MersenneTwister(123)
m = 100
n = 200
s = 10

A = 1/sqrt(100)*randn(rng, m, n)
L = 2*opnorm(A,2)^2
x = zeros(n)
x[shuffle(rng, 1:n)[1:s]] .= randn(rng, 10)
y = A*x

@testset "OMP" begin
    z = omp(y, A, s=s)
    @test z ≈ x
end

@testset "FISTA" begin
    z = fista(y, A, 0.01, L, maxiter=500)
    @test norm(z-x)/norm(x)<=7e-3 # convergence is very slow (this should always be satisfied)
end

@testset "Homotopy" begin
    z = homotopy(y, A, maxiter=2*s)
    @test norm(z-x)<=1e-6
end

@testset "D-AMP" begin
    @testset "SoftThreshold" begin
        z = denoisingAMP(y, A, SoftThreshold())
        @test norm(x-z)/norm(x)<=1e-5
      end
    @testset "GaussianFilter" begin
        z = denoisingAMP(y, A, GaussianFilter())
        @test_broken norm(x-z)/norm(x)<=1e-3
    end
end

@testset "different basis" begin
    y = A*idct(x)
    z = omp(y, LinearInverseProblems.transform(A, dct), s=s)
    @test z ≈ x
end

@testset "Landweber+IRLS" begin
    rng = Random.MersenneTwister(1)
    d = 10
    A = randn(rng, d, d)
    L = norm(A)^2
    x = d*rand(rng, d)
    y = A*x + randn(rng, d)*0.01/norm(x)
    z = landweber(y, A, 1/L*4, maxiter=500)
    @test norm(x-z)/norm(x)<=4e-2
    z = irls(y, A, maxiter=100)
    @test norm(x-z)<=1e-3
end
end # testset Single measurement

@testset "Rembo" begin
    rng = Random.MersenneTwister(2)
    A = 1/sqrt(20)*randn(rng, 20, 30)
    X = zeros(30, 5)
    X[shuffle(rng, 1:30)[1:7], :] = randn(rng, 7, 5)
    Y = A * X
    @testset "OMP" begin
        Z = rembo(Y, A, ε=10e-5, K=7, S=OMP(zeros(size(A,1)), A, s=7), maxiter=5)
        @test norm(Z-X)<=1e-7
    end

    @testset "Homotopy" begin
        Z = rembo(Y, A, ε=10e-5, K=7, S=Homotopy(zeros(size(A,1)), A, maxiter=7), maxiter=5)
        @test norm(Z-X)<=1e-7
    end

    @testset "FISTA" begin
        Z = rembo(Y, A, ε=10e-5, K=7, S=FISTA(zeros(size(A,1)), A, 0.005, maxiter=100), maxiter=10)
        @test_broken norm(Z-X)<=1e-1
    end
end
end # testset Solvers
