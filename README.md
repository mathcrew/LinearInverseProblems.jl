# Linear Inverse Problems

[![pipeline status](https://gitlab.com/mathcrew/LinearInverseProblems.jl/badges/master/pipeline.svg)](https://gitlab.com/mathcrew/LinearInverseProblems.jl/commits/master)
[![coverage report](https://gitlab.com/mathcrew/LinearInverseProblems.jl/badges/master/coverage.svg)](https://gitlab.com/mathcrew/LinearInverseProblems.jl/commits/master)

## Documentation

https://mathcrew.gitlab.io/LinearInverseProblems.jl

## Installation

To have the package always available one can use the built in package manager from `julia` to install it. First you have to download this repo. Then add the Package to the manifest
```
pkg> dev ~/path/to/LinearInverseProblems.jl
```
This only needs to be done the first time. To update the package just use `git`.

Then to start it use `using LinearInverseProblems` and to run the tests
```
pkg> test LinearInverseProblems
```
