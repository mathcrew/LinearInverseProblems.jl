using PkgBenchmark
import LinearInverseProblems

pkgpath = abspath(joinpath(@__DIR__, "../"))
println(pkgpath)
res = benchmarkpkg(pkgpath)

cpu_info = Sys.CPU_NAME * string(Sys.CPU_THREADS) * "threads"
export_dir = joinpath(pkgpath, "benchmark/results/", cpu_info)
export_file = "$(PkgBenchmark.commit(res))-$(PkgBenchmark.juliacommit(res)).md"
if !isdir(export_dir)
    mkpath(export_dir)
end

export_markdown(joinpath(export_dir, export_file), res)
