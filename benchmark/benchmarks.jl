using Base.Iterators
using Random, LinearAlgebra
using LinearInverseProblems
using BenchmarkTools

const SUITE = BenchmarkGroup()

SUITE["solvers"] = BenchmarkGroup(["solvers"])

m = 100
n = 200
s = 10

A = 1/sqrt(100)*randn(m,n)
L = 2*opnorm(A,2)^2
x = zeros(n)
x[shuffle(1:n)[1:s]] .= randn(10)
y = A*x

for s in (OMP, FISTA, Homotopy)
    solver = s(y, A)
    SUITE["solvers"][string(s)] = @benchmarkable collect(take($solver, 5))
end
